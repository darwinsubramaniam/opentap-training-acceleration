﻿// Author: MyName
// Copyright:   Copyright 2020 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Math
{
    [Display("Add", Group: "Math", Description: "Addition TestStep.")]
    public class AddTestStep : TestStep
    {
        #region Settings

        [Display("First Value",Description:"First Value + Second Value",Order: 1000)]
        public int FirstValue { get; set; }

        [Display("Second Value", Description: "First Value + Second Value", Order: 1001)]
        public int SecondValue { get; set; }

        [Display("Select Run Verdict", Description: "Verdict to be evaluate in Run")]
        public OpenTap.Verdict SelectedVedict { get; set; }

        [Browsable(false)]
        [Output]
        public int Sum { get; private set; }

        #endregion

        public AddTestStep()
        {
            SelectedVedict = OpenTap.Verdict.NotSet;
            Rules.Add(() => !FirstValue.Equals(null), errorMessage: "Should not be left null", nameof(FirstValue));
            Rules.Add(() => !SecondValue.Equals(null), errorMessage: "Should not be left null", nameof(SecondValue));
            //var rule = Rules.Where(x => x.ErrorMessage == "").ToArray()[0];
            //rule.IsValid();
        }




        public override void Run()
        {

            // ToDo: Add test case code.
            //RunChildSteps(); //If the step supports child steps.

            Sum = FirstValue + SecondValue;

            Log.Debug($"{FirstValue} + {SecondValue} = {Sum}");

            RunChildSteps();

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            this.UpgradeVerdict(OpenTap.Verdict.Pass);
            
        }
    }
}
