﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Attribute
{

    public enum MobileNetwork
    {
        // IN GU
        [Display("LTE")]LTE,
        [Display("5G")] G5,
        [Display("3G")] G3,
        [Display("2G")] G2,
    }


    [Display(Name: "EnableIfTestStep",
        Groups: new[] { "Training", "Attributes", "Enable If" },
        Description: "Test Step which uses enableif Attribute.")]
    public class EnableIfTestStep : TestStep
    {

        #region Settings

        // Exlplain on the group as well.
        [Display("Should Scale to 10x", Group: "Scaling", Order: 2)]
        public bool isChecked { get; set; } = false;

        [Display("Choose Your Network", Group:"Network", Order:2)]
        public MobileNetwork networkSelection { get; set; } = MobileNetwork.LTE;

        /// <summary>
        /// Create button only when the scaling setting is true.
        /// </summary>
        [Display("Scaling Setting", Group:"Scaling",Order:1)]
        [Browsable(true)]
        [EnabledIf(nameof(isChecked),true,HideIfDisabled =true)]
        public void scalingSetting()
        {

        }

        // <summary>
        /// Create button only when the 2G Selected
        /// </summary>
        [Display("2G Setting", Group: "Network", Order: 1)]
        [Browsable(true)]
        [EnabledIf(nameof(networkSelection), MobileNetwork.G2, HideIfDisabled = true)]
        public void G2Setting()
        {

        }

        // <summary>
        /// Create button only when the 2G Selected
        /// </summary>
        [Display("3G Setting", Group: "Network", Order: 1)]
        [Browsable(true)]
        [EnabledIf(nameof(networkSelection), MobileNetwork.G3,HideIfDisabled =true)]
        public void G3Setting()
        {

        }

        // <summary>
        /// Create button only when the 2G Selected
        /// </summary>
        [Display("5G/LTE Setting", Group: "Network", Order: 1)]
        [Browsable(true)]
        [EnabledIf(nameof(networkSelection), MobileNetwork.G5,MobileNetwork.LTE, HideIfDisabled = true)]
        public void LatestNetworkSetting()
        {

        }

        #endregion

        public EnableIfTestStep()
        {
            // ToDo: Set default values for properties / settings.
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            // UpgradeVerdict(Verdict.Pass);
        }
    }
}
