﻿// Author: MyName
// Copyright:   Copyright 2020 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Attribute
{
    [Display("AttributeRichTestStep", Group: "Attribute", Description: "Insert a description here")]
    public class AttributeRichTestStep : TestStep
    {
        #region Settings

        #region Layout Attribute
        [Layout(LayoutMode.FloatBottom)]
        [Display("Float Bottom Button")]
        [Browsable(true)]
        public void IsDialogFloatBottom()
        {
            Log.Debug("YOU SEE ME AT THE BOTTOM, IM A Button.");
        }


        [Layout(LayoutMode.Normal)]
        [Display("Normal Button", Order: 2)]
        [Browsable(true)]
        public void IsDialogNormal()
        {

        }

        [Display(Name:"Floating Text box",Description:"Floating Text Box Bottom")]
        [Layout(LayoutMode.FloatBottom)]
        [HelpLink("Set Some Hint Text")]
        public string TextBoxFloat
        {
            get;
            set;
        }

        [Layout(LayoutMode.FullRow)]
        [Display(Name: "Full Row Text Box", Description: "Full Row Text Box", Order:3)]
        public string TextBoxFullRow
        {
            get;
            set;
        }

        [Layout(LayoutMode.Normal)]
        [Display(Name: "Normal Text Box", Description: "Normal Text Box", Order: 4)]
        public string TextBoxNormal
        {
            get;
            set;
        }
        #endregion end Layout Attribute

        #region Path Attribute to open file or place to store something.
        // Open dialog for files
        [FilePath]
        [Display("Setting File",Description:"Get the setting file for the testplan run.", Order:5)]
        public string SettingFile{get;set;}


        // This will create a button that opens a dialog for browsing for files. This dialog will also filter for CSV files, with 'any files' filter being optional.
        [FilePath(FilePathAttribute.BehaviorChoice.Open,fileExtension: "csv")]
        [Display("CSV File", Description: "Get CSV FILE.", Order:6)]
        public string CSVFile { get; set; }


        // Will create a button to open a window dialog to look for files.
        [DirectoryPath]
        [Display("Open Directory", Description: "Get CSV FILE.", Order: 7)]
        public string DirectoryPath { get; set; }


        #endregion Path Attribute

        #region Unit Attribute 
        [Unit(Unit:"Hz", UseEngineeringPrefix:false)]
        [Display("Frequenzy in Hz but not ENG Prefixed", Description: "Attribute with Unit but no prefix", Order:8)]
        public double FrequencyWithHz { get; set; }

        [Unit("Hz", UseEngineeringPrefix: true)]
        [Display("Frequenzy in Hz with ENG Prefixed", Description: "Attribute with Unit and prefix", Order:9)]
        public double FrequencyWithHzAndEngPrefix { get; set; }

        [Unit(" Hz", UseRanges: true)]
        [Display("Hz in array", Description: "Attribute to show in array.", Order:11)]
        public int[] MyIntArrayUnits { get; set; }
        #endregion end of Unit Attribute



        // ToDo: Add property here for each parameter the end user should be able to change
        #endregion

        public AttributeRichTestStep()
        {


            // Setting up the Prefix
            FrequencyWithHz = 1234567.89;
            FrequencyWithHzAndEngPrefix = 1234567.89;
            MyIntArrayUnits = new int[] { 1, 2, 4, 5, 6, 7 };
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            // UpgradeVerdict(Verdict.Pass);
        }
    }
}
