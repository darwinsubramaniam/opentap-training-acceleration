module.exports = {
    title: 'OpenTAP Training',
    description: 'OpenTAP Training Material and source codes.',
    dest: '../public',
    base: '/opentap-training-acceleration',

    themeConfig: {
        repo: 'https://gitlab.com/darwinsubramaniam/opentap-training-acceleration',
        editLinks: true,
        editLinkText: 'Help improve this page!',
        docsDir: 'Documentation',
        nav: [
            { text: 'OpenTAP', link: 'https://gitlab.com/opentap/opentap' },
            { text: 'OpenTAP Homepage', link: 'https://www.opentap.io' }
        ],
        sidebar: [

            {
                title: "Session 1",
                sidebarDepth: 6,
                children: [
                    ['TrainingMaterials/Sessions/Session1/Architecture.md', 'OpenTAP Architecture'],
                    {
                        title: "OpenTAP Overview",
                        sidebarDepth: 3,
                        children: [
                            ['TrainingMaterials/Sessions/Session1/Overview/product_comparison.md', 'Product Comparison'],
                            ['TrainingMaterials/Sessions/Session1/Overview/executables.md', 'Executables'],
                            ['TrainingMaterials/Sessions/Session1/Overview/plugins.md', 'Plugin'],
                            ['TrainingMaterials/Sessions/Session1/Overview/testplans.md', 'Test Plans'],
                            ['TrainingMaterials/Sessions/Session1/Overview/teststeps.md', 'Test Steps'],
                            ['TrainingMaterials/Sessions/Session1/Overview/resultlisteners.md', 'Result Listeners']

                        ],


                    },
                    {
                        title: "Getting_Started",
                        sidebarDepth: 3,
                        children: [
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/OpenTAP_Installation_Windows.md', 'OpenTAP_Installation_Windows'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/OpenTAP_Installation_Ubuntu.md', 'OpenTAP_Installation_Ubuntu'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Editor.md', 'Editor'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Basic_test_steps.md', 'Basic_test_steps'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Flow_control_test_steps.md', 'Flow_control_test_steps'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Plugin_Installation.md', 'Plugin_Installation'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Run_Explorer.md', 'Run_Explorer'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Result_Viewer.md', 'Result_Viewer'],
                            ['TrainingMaterials/Sessions/Session1/Getting_Started/Timing_Analyzer.md', 'Timing_Analyzer'],

                        ],


                    },
                    {
                        title: "Homework",
                        sidebarDepth: 3,
                        children: [
                            ['TrainingMaterials/Sessions/Session1/Homework/Lab_Communicate_with_Instrument.md', 'Lab_Communicate_with_Instrument'],
                            ['TrainingMaterials/Sessions/Session1/Homework/Lab_Timing_Analyzer.md', 'Lab_Timing_Analyzer']
                        ]
                    },

                ],




            },

            {
                title: "Session 2",
                sidebarDepth: 6,
                children: [
                    ['TrainingMaterials/Sessions/Session2/Objectives.md', "Objectives"],
                    {
                        title: "Plugin Development",
                        sidebarDepth: 3,
                        children: [
                            ['TrainingMaterials/Sessions/Session2/Plugin_Development/PluginDevelopment.md', 'Prerequisites'],
                            ['TrainingMaterials/Sessions/Session2/Plugin_Development/ObjectHierarchy.md', 'Object Hierarchy'],
                            ['TrainingMaterials/Sessions/Session2/Plugin_Development/TestStepPlugin.md', 'TestStep Plugin'],
                            ['TrainingMaterials/Sessions/Session2/Plugin_Development/ResultListenerPlugin.md', 'ResultListenerPlugin']
                        ]
                    },
                    ['TrainingMaterials/Sessions/Session2/Get_Ready_For_Session3.md', "Get Ready for Session 3"],
                ]
            }

        ]
    },

}

