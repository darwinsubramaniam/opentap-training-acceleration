

# Prerequisites

- Visual Studio 2017 or later

- PathWave Test Automation with OpenTAP SDK (included in PathWave Test Automation installation)

- Basic knowledge of using Pathwave Test Automation Editor.

- Basic knowledge in C#