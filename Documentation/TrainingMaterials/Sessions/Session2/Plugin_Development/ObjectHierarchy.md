# Object Hierarchy

As a start of OpenTAP plugin development, we should have an overview of plugin class hierarchy. The plugins that we are going to work on (grey rectangles with dotted border line) will be extending the base classes (in blue).

![obj hierarchy](../../../Images/Session2/ObjectHierarchy/image2020-5-18_18-29-17.png)