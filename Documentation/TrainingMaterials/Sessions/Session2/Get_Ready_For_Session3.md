
There are 2 softwares required to be installed for **session 3** 
- Keysight IO
- Sampling Oscilloscope Software

## Install Keysight IO

1. Download [Keysight IO](https://www.keysight.com/main/software.jspx?cc=SG&lc=eng&nid=-33330.977662&id=2175637&pageMode=CV&platformId=1).<br/>
:::warning VERSION REMINDER
At the time of this training if there was any update in the Keysight IO Libraries Suite, please make sure to install the **Build 18.1.26209.6** 
:::
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/KeysightIO_Version.png)

2. Double click installer to come to welcoming page. Click **Next**.
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/InstallationProcess2.png)

3. **Agree :arrow_forward:  Next** <br/>
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/LicenseAgree.png)

4. **Typical :arrow_forward: Next**<br/>
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/InstallationProcess4.png)

5. Click **Next**. *Optional* Do enable share anonymous diagostics and usage information as it will help **OpenTAP team** to understand the users.<br/>
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/InstallationProcess5.png)

6. **Install** <br/>.
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/InstallationProcess6.png)

7. **Finish and Restart PC** <br/>.
![Keysight IO Libraries Suite](../../Images/Session2/Get_Ready_Session3/InstallationProcess7.png)

## Install Sampling Oscilloscope Software
In Session 3, we are going to work on an **PC-based instrument**. The instrument has simulation mode available which we can use to simulate the real device, with SCPI-server available. 

1. Download [Sampling Oscilloscope](https://www.keysight.com/main/software.jspx?ckey=2028655&lc=eng&cc=SG&nid=-32976.937137&id=2028655&pageMode=CV).<br/>
::: warning VERSION REMINDER
Please install the **Build A.06.70.274** for this training session.<br/>
If at the time installation , newest version is available **please go to tab Previous Versions** to install the exact version as per request.<br/>
The newer version might have some breaking changes with our training material.
:::
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install1.png)
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install1_1.png)

2. Follow the installation process, it will take a while to install all the required dependencies. Be patient, it might take some time.
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install2.png)

3. Fill in name your **name**, **company**  *e.g* : *Keysight* :arrow_forward: **Next**.
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install3.png)

4. **Complete** :arrow_forward: **Next**.
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install4.png)

5. Once everything is installed, please search for **Keysight FlexDCA** in start menu. <br/> 
Open the application. <br/>
Below is how the main application will look like.
![Sampling Oscilloscope](../../Images/Session2/Get_Ready_Session3/FlexDCA_Install5.png)


## Setup Instrument

### Enable SCPI-server

We will need to start up by setting up the communication between the PC-based instrument with our PC using SCPI-server.

1. Ensure SCPI server is enabled.
Launch **FlexDCA** software.
Select **Tools** :arrow_forward: **SCPI Programming Tools** :arrow_forward: **SCPI Server Setup**.
![Setup](../../Images/Session2/Get_Ready_Session3/Setup1.png)
<br/>
<br/>
2. Ensure the communication that you intend to use is enabled.
![Setup](../../Images/Session2/Get_Ready_Session3/Setup2.png)

### Connect SCPI-server with Keysight Connection Expert
1. Launch Connection Expert.<br/> Right click on **IO** (short cut at right bottom of PC) and click **Connection Expert**.<br/>
![Connect](../../Images/Session2/Get_Ready_Session3/connect1.png)
2. Click **:heavy_plus_sign: ADD** :arrow_forward: **LAN Instrument**
![Connect](../../Images/Session2/Get_Ready_Session3/connect2.png)

3. Select **Enter Address** tab.<br/>
Enter **IP** address as *localhost*.<br/>
Select **HiSlip** :arrow_forward: **Test This VISA Address**.<br/>
![Connect](../../Images/Session2/Get_Ready_Session3/connect3.png)

4. When configuring instrument in OpenTAP, you may find this address in the VISA address list.
![Connect](../../Images/Session2/Get_Ready_Session3/connect4.png)


::: danger Problem Installing
If you encounter any problem installing any of the software, please contact your training representative.
::: 