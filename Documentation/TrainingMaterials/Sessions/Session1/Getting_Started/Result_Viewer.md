**Result viewer** is used to visualize the data from the testing and further analyze them, it is belonging to one of the resource that is used in PathWave Test Automation. It is used to process the stream of data collected from the testing and generate kind of result data for post processing. Multiple result listeners can be configured to operate concurrently due to they are ruining in different threads.

# Configure Result Listeners

![GUI](../../../Images/Session1/ResultViewer/gif-2020-1.gif)

# Create & Execute TestPlan

Add 3 Sine Result Steps from Demonstration package.
![GUI](../../../Images/Session1/ResultViewer/gif-2020-2.gif)

# Invoke Results Viewer

There are several ways to launch result viewer.

1. From Editor > Tools > Results Viewer.

![GUI](../../../Images/Session1/ResultViewer/image2020-1.png)

2. Run ResultsViewer.exe in %TAP_PATH%\

![GUI](../../../Images/Session1/ResultViewer/image2020-2.png)

3. Run from tap CLI

Launch Result Viewer
```cs
"%TAP_PATH%/tap" resultsviewer
```
Note: Running results viewer from Editor has the advantage of viewing result of latest run. Alternatively, result can be loaded from CSV file thru File > Import.

![GUI](../../../Images/Session1/ResultViewer/gif-2020-3.gif)

# Template creation

User can choose to save current chart setting using the steps below.

1. Save current chart setting as template. Template > Save As.

![GUI](../../../Images/Session1/ResultViewer/image2020-3.png)