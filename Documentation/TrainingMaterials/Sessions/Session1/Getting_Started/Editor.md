There are two editors:

- Editor.exe (Enterprise and Community version on Windows)
- TUI (open source cross-platform text-based user interface for usage in terminals (beta))

Our point of discussion in this training is on Editor.exe. There are two ways to launch the editor. One way is to run the Editor.exe (can be accessed from Start > Keysight Test Automation Platform). Another way is from the tap CLI:

Launch Editor:

```cs
"%TAP_PATH%/tap" editor
```

## Components

![Components](../../../Images/Session1/Editor/image2020-1.png)

## Menu bar

![Menubar](../../../Images/Session1/Editor/image2020-2.png)

## Test Plan Panel & Step Settings Panel

### Panel Components

![Panel](../../../Images/Session1/Editor/image2020-3.png)

### Right click on panel

![Panel2](../../../Images/Session1/Editor/image2020-4.png)

## Log Panel

### Color code that indicates severity

![Panel2](../../../Images/Session1/Editor/image2020-5.png)

### Right click menu

![Panel2](../../../Images/Session1/Editor/image2020-6.png)

## TUI

TUI is an open source cross-platform text-based user interface for usage in terminals (beta version)

- Installation

Method 1: Install from %TAP_PATH%\PackageManager.exe on Windows OS

Method 2: Download from Sandbox(OpenTAP Internal packages)

Installation:

```cs
Navigate to download package location
On Windows:
   Double click on the package file
 Or run tap cli
  "%TAP_PATH%/tap" package install TUI.<version>.TapPackage
On linux:
   "$TAP_PATH/./tap" package install TUI.<version>.TapPackage
```

- Method 3: Install from tap CLI:

```cs
On Windows: "%TAP_PATH%/tap" package install TUI --version any
On linux: "$TAP_PATH/./tap" package install TUI --version any
```

- Run

```cs
On Windows: "%TAP_PATH%/tap" tui
On Linux: "$TAP_PATH/./tap" tui
```
- GUI is almost the same as Editor.exe, except, there is no panel for available test steps.

![GUI](../../../Images/Session1/Editor/image2020-7.png)