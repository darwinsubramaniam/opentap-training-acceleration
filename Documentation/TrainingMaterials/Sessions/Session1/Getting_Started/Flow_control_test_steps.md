# **Flow Control Test Steps**

# If Verdict

Runs its child steps only when the verdict of another step has a specific value.
Determined during test plan execution.
Test plan's verdict will be the most severe verdict of the test steps.
Typical verdict behavior of the parent step is the same as Test plan.

e.g. Sequential step. However, parent step has the right to decide the verdict.

User can choose

- Run Child steps
- break
- continue the loop (looping step will be described below),
- abort test plan
- Wait For User (a dialog box will be prompted for user decision).

![GUI](../../../Images/Session1/TestSteps/image2020-14.png)

# Parallel

Runs its child steps in parallel in separate threads.

Below example test plan of having two delay steps with first to delayed by 5s, and second delayed by 10s. The total running time for the test plan is 10s as both the delay steps are running in parallel.

![GUI](../../../Images/Session1/TestSteps/image2020-15.png)

# Lock

Locks the execution of child steps based on a specified mutex.

Below example extended from the Parallel steps test plan, both the delay steps are setup to 5s, and they are parked under Lock step. It depends on which ever Lock step (Lock1 or Lock2) starts to execute, while others have to wait for the completion of the first started. Thus, the two child steps which park under the parent step, Parallel are no longer running in parallel, The whole running time will be 10s instead of 5s.

![GUI](../../../Images/Session1/TestSteps/image2020-16.png)

# Repeat

Repeats its child steps either a fixed number of times or until the verdict of a specified step changes to a specified state.

![GUI](../../../Images/Session1/TestSteps/image2020-17.png)

Three modes be supported

- Fixed Count: Repeat iteration a fixed number of times

- While: Repeat while the specified condition is met. This guarantees the child step execute a minimum of one time

- Until: Repeat until the specified condition is met. This guarantees the child step execute a minimum of one time

# Sequence

Runs its child steps sequentially.

Below example of a series of steps which helps to initialize instrument.

![GUI](../../../Images/Session1/TestSteps/image2020-18.png)

# Legacy → Sweep Loop

Loops all of its child steps while sweeping specified parameters/settings on the child steps. Exception: When a child step is a Test Plan Reference, the parameters/settings inside the referenced test plan are NOT swept.

![GUI](../../../Images/Session1/TestSteps/image2020-19.png)

Two Sweep modes be supported

- Flow Control Test Steps#Within Run: Loop through the sweep values in a single TestPlan run.
- Flow Control Test Steps#Across Runs: Loop through the sweep values between runs.

# Legacy → Sweep Loop (Range)

Loops all of its child steps while sweeping a specified parameter/setting over a range, with either linear or exponential steps. Exception: When a child step is a Test Plan Reference, the parameter/setting inside the referenced test plan is NOT swept.

![GUI](../../../Images/Session1/TestSteps/image2020-20.png)

Two Sweep Range be supported

- Linear: Create sweep range with Linear growth function.
- Exponential: Create sweep range with Exponential growth function.

# Sweep loop

Table based loop that sweeps the value of its parameters based on a set of values.

Setup Temperature to be the parameter to be swept:

![GUI](../../../Images/Session1/TestSteps/image2020-21.png)

![GUI](../../../Images/Session1/TestSteps/image2020-22.png)

Setup the temperature table:

![GUI](../../../Images/Session1/TestSteps/image2020-23.png)

# Sweep Range

Range based sweep step that iterates value of its parameters based on a selected range.

Setup Temperature to be the parameter to be swept:

![GUI](../../../Images/Session1/TestSteps/image2020-24.png)

![GUI](../../../Images/Session1/TestSteps/image2020-25.png)

Setup the temperature range. We can setup Linear or exponential range:

![GUI](../../../Images/Session1/TestSteps/image2020-26.png)

# Test Plan Reference

References a test plan from an external file directly, without having to store the test plan as steps in the current test plan.

Maximum level reference is limited to 16.

![GUI](../../../Images/Session1/TestSteps/image2020-27.png)

## ***Within Run***

When within run is selected, the child step will be repeated with the sweep values specified in the list. Here, an example of sweeping log message from "1" to "3".

![GUI](../../../Images/Session1/TestSteps/image2020-28.png)

![GUI](../../../Images/Session1/TestSteps/image2020-29.png)

## ***Across Runs***

When sweep mode is setup to across run, the sweep value in the list will be swept one at a time for each run. Here is an example of setting the outer loop to Across Runs mode, while the inner sweep loop is within run. On the first execution, Log Severity = Information is swept. On second run, Warning is used as Log Severity ...

![GUI](../../../Images/Session1/TestSteps/image2020-30.png)

![GUI](../../../Images/Session1/TestSteps/image2020-31.png)

