Test plan **run explorer** extracts data from database. Same as Result Viewer, the test plan run explorer can be launched in three ways. Both result viewer and test plan run explorer are independent application in where they will remain open even editor is closed. When the result of a run instance is selected, the icons in the tool bar are used to carry out different tasks.

![GUI](../../..//Images/Session1/RunExplorer/image2020-1.png)

# Select Runs or Results

![GUI](../../../Images/Session1/RunExplorer/gif-1.gif)

# Select or Add a Database

![GUI](../../../Images/Session1/RunExplorer/gif-2.gif)

# Working with Test Plan Runs

Reference detail from Section Working with Test Plan Runs of Keysight Test Automation Help.

## Open TestPlan in OpenTap

![GUI](../../../Images/Session1/RunExplorer/gif-3.gif)

## Compare two selected Test Plans

![GUI](../../../Images/Session1/RunExplorer/gif-4.gif)

## Compare two selected logs

![GUI](../../../Images/Session1/RunExplorer/gif-5.gif)

## Visible/Invisible Optional Test Run Parameters

![GUI](../../../Images/Session1/RunExplorer/gif-6.gif)

## Search Test Run

The test run can be searched by applying the search criteria(based on the test run parameters), such as, the owner of the test ...

![GUI](../../../Images/Session1/RunExplorer/gif-7.gif)

## Plot Selected Test Run Result

![GUI](../../../Images/Session1/RunExplorer/image2020-2.png)

- Plot Selected: Plot test result in Result viewer
- Plot Using Template: Plot test result in Result viewer with a previously saved template
- Delete Selected Runs: Remove the selected test result from Database
- Add Tag...: Add a tag which can be used in Result View

![GUI](../../../Images/Session1/RunExplorer/gif-8.gif)

# Working with Test Plan Results

Reference detail from Section Working with Test Plan Results of Keysight Test Automation Help.

## Plot Test Result

All test results are plotted on the Result view, then, the particular test result can be filtered out.

![GUI](../../../Images/Session1/RunExplorer/gif-9.gif)

## Search Test Result

The test result can be searched by applying the search criteria(based on the test run parameters), such as, the owner of the test ...

![GUI](../../../Images/Session1/RunExplorer/gif-10.gif)

## Export Result

Test result export to the format supported by result listeners.

![GUI](../../../Images/Session1/RunExplorer/gif-11.gif)

## Add a Limit Set to Results

Limit Sets to define and apply a limit set to an existing set of results

![GUI](../../../Images/Session1/RunExplorer/gif-12.gif)

## Add a Limit Set condition to Results

![GUI](../../../Images/Session1/RunExplorer/gif-13.gif)

