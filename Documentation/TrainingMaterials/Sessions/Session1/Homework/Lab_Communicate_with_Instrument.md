We are going to communicate with a virtual instrument in this lab.

1. Configure FlexDCA as stated in [Get Ready for Session 3](../../Session2/Get_Ready_For_Session3.md).

2. Add Generic SCPI Instrument as instrument. Refer [Test plans](../../Session1/Overview/testplans.md) 

![GUI](../../../Images/Session1/homework/image2021-5.png)

3. Configure VISA address of FlexDCA.

![GUI](../../../Images/Session1/homework/image2021-6.png)

4. Add "Basic Steps → SCPI” and you may see the relevant instrument is selected.

![GUI](../../../Images/Session1/homework/image2021-7.png)

5. Run Test plan. Observe the log.

![GUI](../../../Images/Session1/homework/image2021-8.png)



